## Welcome to EN-SMM-APC Team!
##### EN | Engineering
##### SMM | Survey, Mechatronics and Measurements
##### APC | Acquisition, Processing and Control software

### Overview about our mission:

The section is in charge of the architecture, design, implementation, testing,
deployment and maintenance of the **software for the survey,
for the acquisition & control of the Beam Intercepting Devices,
the survey permanent monitoring and alignment systems** as well as the RADMON devices.
The section is also responsible for the nTOF DAQ software and operation,
provides software support to the UA9 SPS experiment and technical support
to the informatics infrastructure of the group and the STI group cluster.


About Acquisition:
- Survey field measurements (i.e. [TSUNAMI](https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=22152983))
- Remote Monitoring ([MATHIS](https://readthedocs.web.cern.ch/display/SUS/MATHIS+Software), ...)
- [nTOF DAQ](https://project-ntof.web.cern.ch/project-ntof/)
- [UA9](https://home.cern/science/experiments/ua9) detectors acquisition
- BIDs monitoring

About Processing:
- Survey field processing (i.e. [LGC2](https://readthedocs.web.cern.ch/display/SUS/LGC2+User+Guide))
- [GEODE](https://readthedocs.web.cern.ch/display/SUS/Geode+User+Guide)
- [Rabot](https://readthedocs.web.cern.ch/display/SUS/Rabot+User+Guide)

About Control Software:
- Survey remote alignment systems
- BIDs control

##### Useful Links about APC
- [EN-SMM-APC Documentation Stream](https://apc-dev.web.cern.ch/docs/)
- [GitLab](https://gitlab.cern.ch/apc)
- [JIRA - APCSUPPORT](https://its.cern.ch/jira/projects/APCSUPPORT/summary)
- [Web Development Hints](WEBDEV.md)

### n_ToF Experiment - The neutron time-of-flight facility

n_TOF is a pulsed neutron source coupled to a 200 m flight path designed
to study neutron-nucleus interactions for neutron kinetic energies ranging
from a few meV to several GeV. The neutron kinetic energy is determined
by time-of-flight, hence the name n_TOF.

Within our group we develop the DAQ software that is a GUI - Web based
for the Data Acquisition System. This software is able toable to configure
more than 150 DAQ channels as well as to set the Experimental Area Configuration
according to the specific experiment. We manage interaction with Oracle DB in order to
store critical information for DAQ operations and data analysis. We also store the data
over [CASTOR](http://castor.web.cern.ch/).

##### Useful Links about n_ToF

- [Public Cern Website about n_ToF](https://ntof-exp.web.cern.ch/ntof-exp/)
- [Our Code for n_ToF on GitLab](https://gitlab.cern.ch/apc/experiments/ntof)
- [Base-Vue Examples](https://apc-dev.web.cern.ch/base-vue)
- [StandAlone DAQ (Beta)](https://ntof-daq.web.cern.ch/standalone/beta/#/)
- [StandAlon DAQ (Production!!)](https://ntof-daq.web.cern.ch/standalone)
- [Monitoring Dashboard](http://project-ntof.web.cern.ch/project-ntof/monitoring/monitoring.php?did=true)
- [nToF DAQ Legacy Website](https://project-ntof.web.cern.ch/project-ntof/)
- [TWiki Collaboration web](https://twiki.cern.ch/twiki/bin/viewauth/NTOF/WebHome)
- [JIRA - NTOFNEWDAQ](https://its.cern.ch/jira/projects/NTOFNEWDAQ/summary)
- n_ToF AFS ->  `/afs/cern.ch/work/n/ntofdev`

###### Access to Oracle:
- Request access to e-group [oracle-general-purpose-users](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=264002)
- Follow [this GUIDE](https://cern.service-now.com/service-portal/article.do?n=KB0000829) to create an Oracle Account
- We have already the following service accounts:
    - `ntof` on `cerndb1`
    - `ntofdev` on `devdb11` (our pre-prod)
    - `ntoftest` on `devdb11` (our alpha)
- [Tools for Oracle Databases](https://indico.cern.ch/event/672720/contributions/2756246/attachments/1561098/2457932/Tools_for_Oracle_Databases.pdf)
- [Oracle Documentation](http://oracle-documentation.web.cern.ch/oracle-documentation/)
- [Database Tutorials](https://indico.cern.ch/event/672720/)

###### Access n_ToF experimental area:
- [Ask for a dosimeter](https://dosimetry.web.cern.ch/en)
- Permissions to ask:
[NTOF-EXP](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:2100:7359641384601::NO:2100:PERMISSION_ID:19),
[NTOF-CR](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:2100:7359641384601::NO:2100:PERMISSION_ID:20),
[0547-R-001](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:2100:7359641384601::NO:2100:PERMISSION_ID:3968)

###### Tunnel Prevessin - Meyrin Transit Authorization
[Here](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:2100:16764942103946::NO:RP:PERMISSION_ID:7)
you can request access to CERN Prevessin - Meyrin Tunnel Entrance (anti congestion entrance)
