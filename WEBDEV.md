# Web Development Hints

This document briefly introduces and present the various tools that are being used in our web-oriented developments (Node.js, front-end ...).

It is **not** intended to be exhaustive or complete, it just aggregates a couple tips and tricks, please refer to the dedicated documentation for more advanced details.

For a more formal presentation a [guideline](https://mro-dev.web.cern.ch/docs/std/en-smm-apc-web-guidelines.html) is available.

The following project can be used as a reference for front-end library develoments:
 [@en-smm/base-vue](https://gitlab.cern.ch/mro/common/www/base-vue).

## Linting: ESLint & JSHint

Both front-end and back-end projects uses [JSHint](https://jshint.com/about/) and [ESLint](https://eslint.org) for linting.

An helper script is available and can be invoked as:
```shell
npm run lint

# Parser friendly output
npx lint --builder
# or ./bin/lint --builder for not up to date projects

# Using eslint fix mode
npx lint --fix
```

**Note:** Special care must be brought to *Vue.js* based projects, to properly parse `".vue"` files (additional plugins and command line arguments).

## TypeChecking: TypeScript

Both front-end and back-end projects uses [TypeScript](https://www.typescriptlang.org/) for TypeChecking.

A non-intrusive, comments only ("a la Doxygen") styling is used.

**Note:** Special care must be brought to *Vue.js* based projects which can't directly be parser by *tsc*, for those files the TypeChecker is invoked by *ESLint*.

To run the TypeChecker:
```bash
./bin/tc

# or
npx tsc
npx tsd
```

For additional information see [TypeChecking hints](TypeChecking.md)

## Testing

Tests are always located in a *test* directory, there can be several *test* directories depending on the project, for complete web apps there are two:
-   one in the root directory for back-end tests
-   one in a *www* directory for front-end (Karma) tests

### Framework: Mocha/Chai

[Mocha](https://mochajs.org/index.html) is used to organize tests, using `describe`, `it`, `before{Each}` and `after{Each}` functions.

[Chai](https://www.chaijs.com/) is used to make assertions, using `expect` interface, quite often with [dirty-chai](https://www.chaijs.com/plugins/dirty-chai/) plugin.

**Example:**
```javascript
// Sample test file

// ES6 style import (for front-end projects)
import { expect } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
// or Node.js style import (for back-end projects)
const
  { expect } = require('chai'),
  { describe, it, beforeEach, afterEach } = require('mocha');


// used to group tests
describe('FileName tests', function() {
  // do not construct context here, use beforeEach/afterEach or before/after
  var context;

  beforeEach(function() {
    // do prepare the test environement
    context = { test: 42, value: true };
  });

  afterEach(function() {
    // don't forget to release resources
    context = null;
  });

  it('runs a synchronous test', function() {
    expect(context.test).to.be.greaterThan(41);
  });

  it('runs an asynchronous test', function(done) {
    setTimeout(function() {
      expect(context.test).to.be.greaterThan(41);
      done();
    });
  });

  it('runs a promise based test', function() {
    return new Promise(function(resolve, reject) {
      // dirty-chai syntax, we do know that an assertion done here
      expect(context.value).to.be.true();
      // if the promise rejects, then the test will fail
      resolve(42);
    });
  });

  it('runs another promise based test', async function() {
    await new Promise(function(resolve, reject) {
      expect(context.value).to.be.equal(true);
      resolve(42);
    });
  });
});
```

### Back-End tests: mocha

[Mocha](https://mochajs.org/index.html) is directly used to run back-end (Node.js) tests:
```shell
mocha test

# With nodemon to run tests whenever a file is modified
nodemon -w src -w test -x mocha test
npm run test:watch

# Enabling debug and running a specific test:
DEBUG=dim:* mocha test -g 'text...'

# Running using npm-scripts
npm run test

# Run CI tests (runs front-end and back-end tests)
npm run test:ci
```

### Front-End tests: Karma

[Karma](https://karma-runner.github.io/latest/index.html) is used to automate browser testing:
```shell
# Starting karma (WebPack will be started in file-watching mode)
karma start

# Running an 'Headless' browser in single-run mode:
karma start --single-run --browsers ChromiumHeadless

# Running a specific test in debug mode with logging:
DEBUG=redim:* karma start --grep 'my test' --debug
```
