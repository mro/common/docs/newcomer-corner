# Adding TypeChecking to a project

To enable TypeChecking in a project, create a `tsconfig.json` file:
```json
{
  "extends": "@tsconfig/node12/tsconfig.json",

  "include": ["src/**/*"],
  "exclude": ["node_modules", "**/*.spec.ts", "**/*.test-d.ts"],
  "compilerOptions": {
    "allowJs": true,
    "noEmit": true,
    "checkJs": true
  }
}
```

And install the following packages:
```bash
npm install --save-dev @tsconfig/node12 @typescript-eslint/eslint-plugin @typescript-eslint/parser typescript tsd

# Add any relevant typescript library:
npm install --save-dev @types/lodash @types/mocha @types/node @types/q
```

Reuse the scripts (in `bin/*` and `package.json`) and `.eslintrc.yml` from another project (ex: [dim.js](https://gitlab.cern.ch/apc/common/dim.js))

# Running the TypeChecker

To run the TypeChecker:
```js
// typecheck the project itself
npx tsc

// test the declaration file
npx tsd

// or use the CI scripts:
./bin/tc

// this one pre-processes the output to make it more grep friendly (eases IDE integration)
npm run tc:ci:builder
```

# Writing TypeScript/JSDoc

For further details, please refer to
[JSDoc Reference](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html)

Ensure that you did enable *TypeScript* at the top of your *JavaScript* file:
```js
// @ts-check
```

## Functions and Methods signature

Function parameters are declared using `@param` and `@return`, it is recommended
to also add `@brief` and `@details`, providing additional information:
```js
/**
 * @brief generate xml document from javascript object
 * @param  {Element} node
 * @param {XmlJsObject} args
 * @return {Element}
 */
function fromJs(node, args) {
  //...
}
```

**Note**: `?` is equivalent to `|null` and `=` is equivalent to `|undefined`, to
make an argument optional:
```js
/**
 * @param {string|ServiceInfo} service
 * @param {(DnsClient|NodeInfo|string)?=} dns
 * @param {boolean?=} [isRpc=false]
 */
test(service, dns, isRpc = false) {
  // ...
}
```

## Class Properties

Properties in a class are declared when initialized in the constructor:
```js
class A {
  constructor() {
    this.test = 42;

    // Casting type
    /** @type {Array<number>} */
    this.value = [];
    
    /** @type {?number} */
    this.opt = null;
  }
}
```

## Type Casting

Sometimes a type evolves to another, it can thus be casted:
```js
class A {
  

  /**
   * @param {XmlDataInPart} data
   */
  _add(data) {
    if (_.isNil(data.index)) {
      data.index = this._nextIndex(dataset);
    }
    //...
    // we ensured that the partial XmlData is now an XmlData
    this.dataset.push(/** @type {XmlData} */ (data));
  }
```

## Import type only

Types that are used only for the typechecking (ex: argument type), may be
imported using a JSDoc `@typedef`:
```js
/**
 * @typedef {import('dim').ServiceInfo} ServiceInfo
 */
```

**Note**: the import may also refer to a local file

## Internal Declaration Files

It may be convenient to declare types and share it in a single project/library.

To do so simply create a `types.d.ts` (types can be replaced with anything) file:
```js
// replace namespace with directory name, or something relevant
// this won't be exported anyway
export as namespace localNamespace;

export interface MyInterface {
  value: string
  //...
}
```

Import types from those declaration files using JSDoc `@typedef`:
```js
/**
 * @typedef {import('./types').MyInterface} MyInterface
 */
```

# Writing a declaration file (d.ts)

A first template of declaration file can be generated using the *TypeScript* compiler `tsc`

Create a `tsconfig-emit.json` file to generate the declaration:
```json
{
  "extends": "@tsconfig/node12/tsconfig.json",

  "include": ["src/**/*"],
  "exclude": ["node_modules", "**/*.spec.ts"],
  "compilerOptions": {
    "allowJs": true,
    "checkJs": true,
    "declaration": true,
    "emitDeclarationOnly": true,
    "removeComments": true,
    "outDir": "types",
    "outFile": "types.d.ts"
  }
}
```

Then call the *TypeScript* compiler, it should generate a declaration file:
```bash
npx tsc --build "tsconfig-emit.json"
```

**Note:** `dts-gen` may also be used to generate a template declaration file

Once the template is generated, look for the `"index"` module, it should contain
your top-level declarations, modify it accordingly to export a module named as your
library, ex:
```ts
declare module "dim-xml" {
  export class ...
}
```

**Note**: do a major cleanup, removing any '_' prefixed property/methods and
internal types.

## Validating declaration file

To validate the declaration file, create an `src/index.test-d.ts` file (assuming
that your declaration file is `src/index.d.ts`), using `tsd` assertions on types, ex:
```ts
import { expectType } from 'tsd';
// ...

// wrap in async to use await
(async function() {
  var v = new DicValue<string>(new ServiceInfo('test', 'C', null,
    NodeInfo.local('localhost', 1234)));

  expectType<string | void>(await v.promise());
  v.on('value', (value: string|void) => expectType<string|void>(value));
}());
```

Then run `tsd`:
```bash
npx tsd
```

Once validated, export it in `package.json` file:
```json
...
    "main": "src/index.js",
    "types": "src/index.d.ts",
...
```

## JSDoc patch

A bug on current *TypeScript* compiler (4.0.1) makes it fail if you import types from
a JavaScript file to another, displaying messages such as:
```
src/packets/index.js:2:1 - error TS9006: Declaration emit for this file requires using private name 'DnaStream' from module '"/mnt/storage/work/CERN/sources/ntof/dim.js/src/packets/Dna"'. An explicit type annotation may unblock declaration emit.
```

Applying the following patch on your typescript module may temporarily disable this error:
```patch
--- node_modules.old/typescript/lib/tsc.js	1985-10-26 09:15:00.000000000 +0100
+++ node_modules/typescript/lib/tsc.js	2020-10-27 13:39:58.913712859 +0100
@@ -77347,7 +77347,9 @@ var ts;
         function trackSymbol(symbol, enclosingDeclaration, meaning) {
             if (symbol.flags & 262144)
                 return;
-            handleSymbolAccessibilityError(resolver.isSymbolAccessible(symbol, enclosingDeclaration, meaning, true));
+            if (!ts.isInJSFile(enclosingDeclaration)) {
+                handleSymbolAccessibilityError(resolver.isSymbolAccessible(symbol, enclosingDeclaration, meaning, true));
+            }
             recordTypeReferenceDirectivesIfNecessary(resolver.getTypeReferenceDirectivesForSymbol(symbol, meaning));
         }
         function reportPrivateInBaseOfClassExpression(propertyName) {
```

## DTS-Gen patch

Here's a small patch on dts-gen to add arument names on methods and constructors
(TypeScript compiler seems to have evolved and current dts-gen will generate `...args` everywhere):
```patch
--- node_modules,old/dts-gen/bin/lib/index.js
+++ node_modules/dts-gen/bin/lib/index.js
@@ -310,6 +310,21 @@ function getParameterListAndReturnType(o
                 params.push(dts_dom_1.create.parameter('args', dom.type.array(dom.type.any), dom.ParameterFlags.Rest));
             }
         }
+        else if (fn.arguments) {
+          params = fn.arguments.map(p => dts_dom_1.create.parameter(`${p.getText()}`, inferParameterType(fn, p)))
+        }
+        else if (fn.members) {
+          const ctor = fn.members.find(m => m.kind === ts.SyntaxKind.Constructor);
+          if (ctor) {
+            ts.forEachChild(fn, visit);
+            if (ctor.parameters) {
+              params = ctor.parameters.map(p => dts_dom_1.create.parameter(`${p.name.getText()}`, inferParameterType(fn, p)));
+            }
+            else if (ctor.arguments) {
+              params = ctor.arguments.map(p => dts_dom_1.create.parameter(`${p.getText()}`, inferParameterType(fn, p)))
+            }
+          }
+        }
         return [params, hasReturn ? dom.type.any : dom.type.void];
     }
     function visit(node) {
```
