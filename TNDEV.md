# Technical Network Development

This document briefly introduces and present a set of tools that can be used for *BE-CO* [Technical Network](http://information-technology.web.cern.ch/fr/services/Technical-Network)
related developments.

It is **not** intended to be exhaustive or complete, it just aggregates a couple tips and tricks.

It is assumed that you already have a [BE-CO VPC](https://wikis.cern.ch/display/ACCADM/VPC+Virtual+Machines+BE-CO).

## Development Environment

Prebuilt libraries and headers are available on `/afs/cern.ch/work/a/apcdev/tn-builder/dist`.

It is recommended to use a specific path in your BE-CO VPC:
```bash
mkdir -p /opt/gitlab-ci
ln -s /afs/cern.ch/work/a/apcdev/tn-builder/dist /opt/gitlab-ci/dist
```

### Compiling with CMake

To compile a *CMake* based project:
```bash
cmake3 .. -DCMAKE_FIND_ROOT_PATH=/opt/gitlab-ci/dist \
  -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=OFF \
  -DCMAKE_INSTALL_PREFIX= -DCMAKE_INSTALL_LIBDIR=lib
```

### Additional tools

Some additional tools, such as *cquery* are needed for code-completion and linting:
```bash
# As described in https://gitlab.cern.ch/apc/common/distfiles/blob/master/README.md
yum-config-manager --add-repo http://apc-dev.web.cern.ch/distfiles/cc7/APC-Common.repo

yum install cquery
```

### Remote compilation

One can use [x-builder](https://gitlab.cern.ch/apc/common/tools/x-builder) to
run remotely build and execute cquery.

## TN Builder & GitLab-CI

A *TN Builder* has been setup to compile and pre-deploy binaries, libraries and
headers in Technical Network environment.

This machine is a [BE-CO VPC](https://wikis.cern.ch/display/ACCADM/VPC+Virtual+Machines+BE-CO) hooked on gitlab-ci that compiles and deploys builds.

[*GitLab-CI* helper scripts](https://apc-dev.web.cern.ch/ci/scripts/gitlab-ci-tn-builder.yml) can be used to compile and deploy in this environment:
```yml
include
  - 'https://apc-dev.web.cern.ch/ci/scripts/gitlab-ci-tn-builder.yml'

tn-build:
  extends: .tn-cmake-build

tn-deploy:
  extends: .tn-cmake-deploy
```

The deploy step installs artifacts in `/opt/gitlab-ci/dist` on the VPC.

Once a week this directory is synced on `/afs/cern.ch/work/a/apcdev/tn-builder/dist`.

It may also be manually synced by triggering [a CI job](https://gitlab.cern.ch/apc/common/tn-builder/pipelines).
